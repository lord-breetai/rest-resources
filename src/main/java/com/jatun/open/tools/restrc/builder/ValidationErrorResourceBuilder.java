/*
 * Copyright 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jatun.open.tools.restrc.builder;

import com.jatun.open.tools.restrc.model.ValidationError;
import com.jatun.open.tools.restrc.model.ValidationErrorResource;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ivan Alban
 */
public final class ValidationErrorResourceBuilder {

    public static ValidationErrorResourceBuilder getInstance() {
        return new ValidationErrorResourceBuilder();
    }

    private ValidationErrorResourceBuilder() {
    }

    public ValidationErrorResource build(MethodArgumentNotValidException source) {
        BindingResult bindingResult = source.getBindingResult();

        List<ValidationError> content = new ArrayList<>();
        for (FieldError fieldError : bindingResult.getFieldErrors()) {
            content.add(createValidationError(fieldError));
        }

        return new ValidationErrorResource(content, source.getClass(), source.getMessage());
    }

    private ValidationError createValidationError(FieldError fieldError) {
        String modelName = StringUtils.capitalize(fieldError.getObjectName());
        String field = fieldError.getField();

        String validator = null;
        if (null != fieldError.getCode()) {
            validator = StringUtils.uncapitalize(fieldError.getCode());
        }

        ValidationError instance = new ValidationError();
        instance.setModel(modelName);
        instance.setField(field);
        instance.setCode(modelName + "." + field);
        instance.setMessage(fieldError.getDefaultMessage());

        if (!StringUtils.isEmpty(validator)) {
            instance.setValidatorCode(modelName + "." + field + "." + validator);
        }

        return instance;
    }
}
