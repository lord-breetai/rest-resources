/*
 * Copyright 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jatun.open.tools.restrc.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.util.ClassUtils;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Ivan Alban
 */
public class IterableResource<T> {

    private static final String TYPE = "iterable";

    private final Collection<T> content;

    private final String contentClazz;

    public IterableResource(Collection<T> content, Class<T> contentClazz) {
        this.content = new ArrayList<>();
        this.contentClazz = ClassUtils.getShortName(contentClazz);

        if (null != content) {
            this.content.addAll(content);
        }
    }

    @JsonProperty("data")
    public Collection<T> getContent() {
        return content;
    }

    @JsonProperty("@class")
    public String getContentClazz() {
        return contentClazz;
    }

    @JsonProperty("@type")
    public String getType() {
        return TYPE;
    }
}
