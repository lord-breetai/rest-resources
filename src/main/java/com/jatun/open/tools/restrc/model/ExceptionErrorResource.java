/*
 * Copyright 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jatun.open.tools.restrc.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Ivan Alban
 */
public class ExceptionErrorResource extends ErrorResource {

    private static final String TYPE = "exception-error";

    private final ExceptionError content;

    public ExceptionErrorResource(ExceptionError content,
                                  Class<? extends Exception> exceptionClazz,
                                  String message) {
        super(exceptionClazz, message);
        this.content = content;
    }

    @JsonProperty("data")
    public ExceptionError getContent() {
        return content;
    }

    @JsonProperty("@type")
    public String getType() {
        return TYPE;
    }
}
