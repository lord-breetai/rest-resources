/*
 * Copyright 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jatun.open.tools.restrc.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.util.ClassUtils;

/**
 * @author Ivan Alban
 */
public class SingleResource<T> {

    private static final String TYPE = "single";

    private final T content;

    private final String contentClazz;

    public SingleResource(T content) {
        this.content = content;
        this.contentClazz = ClassUtils.getShortName(content.getClass());
    }

    @JsonProperty("data")
    public T getContent() {
        return this.content;
    }

    @JsonProperty("@class")
    public String getContentClazz() {
        return this.contentClazz;
    }

    @JsonProperty("@type")
    public String getType() {
        return TYPE;
    }
}
