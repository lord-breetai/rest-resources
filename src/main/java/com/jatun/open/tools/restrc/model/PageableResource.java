/*
 * Copyright 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jatun.open.tools.restrc.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collection;

/**
 * @author Ivan Alban
 */
public class PageableResource<T> extends IterableResource<T> {

    private static final String TYPE = "pageable";

    private Integer totalPages;

    private Long totalElements;

    public PageableResource(Collection<T> content, Class<T> contentClazz) {
        super(content, contentClazz);
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public void setTotalElements(Long totalElements) {
        this.totalElements = totalElements;
    }

    @JsonProperty("@totalPages")
    public Integer getTotalPages() {
        return totalPages;
    }

    @JsonProperty("@totalElements")
    public Long getTotalElements() {
        return totalElements;
    }

    @JsonProperty("@type")
    @Override
    public String getType() {
        return TYPE;
    }
}
