/*
 * Copyright 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jatun.open.tools.restrc.builder;

import com.jatun.open.tools.restrc.exception.ErrorSource;
import com.jatun.open.tools.restrc.exception.annotations.ErrorMessage;
import com.jatun.open.tools.restrc.exception.annotations.ErrorMessageAttribute;
import com.jatun.open.tools.restrc.model.ExceptionError;
import com.jatun.open.tools.restrc.model.ExceptionErrorResource;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Ivan Alban
 */
public class ExceptionErrorResourceBuilder {

    public static ExceptionErrorResourceBuilder getInstance() {
        return new ExceptionErrorResourceBuilder();
    }

    private ExceptionErrorResourceBuilder() {
    }

    public <T extends Exception> ExceptionErrorResource build(T source) {
        ExceptionError content = null;

        if (source instanceof ErrorSource) {
            content = buildExceptionError((ErrorSource) source);
        }

        if (source.getClass().isAnnotationPresent(ErrorMessage.class)) {
            content = buildExceptionError(source);
        }

        if (null != content) {
            return new ExceptionErrorResource(content, source.getClass(), source.getMessage());
        }

        throw new UnsupportedOperationException("Exception " + source.getClass().getName() + " cannot be processed.");
    }

    private <T extends Exception> ExceptionError buildExceptionError(T source) {
        ExceptionError instance = new ExceptionError();

        ErrorMessage errorMessage = AnnotationUtils.findAnnotation(source.getClass(), ErrorMessage.class);

        if (null == errorMessage) {
            throw new UnsupportedOperationException("Unable to handle " + source.getClass());
        }

        instance.addAttribute("code", errorMessage.code());

        if (!StringUtils.isEmpty(errorMessage.message())) {
            instance.addAttribute("message", errorMessage.message());
        }

        HashMap<String, Serializable> params = new HashMap<>();

        ReflectionUtils.doWithFields(source.getClass(),
                (Field field) -> {
                    ReflectionUtils.makeAccessible(field);

                    ErrorMessageAttribute annotation = field.getAnnotation(ErrorMessageAttribute.class);
                    Object value = ReflectionUtils.getField(field, source);

                    if (null != value && (value instanceof Serializable)) {
                        String key = field.getName();
                        params.put(key, (Serializable) value);
                    }
                },
                field -> field.isAnnotationPresent(ErrorMessageAttribute.class));

        instance.addAttribute("params", params);

        return instance;
    }

    private ExceptionError buildExceptionError(ErrorSource source) {
        Map<String, Serializable> errorData = source.errorData();

        if (null == errorData) {
            errorData = new HashMap<>();
        }

        ExceptionError instance = new ExceptionError();

        for (String key : errorData.keySet()) {
            Serializable value = errorData.get(key);

            instance.addAttribute(key, value);
        }

        return instance;
    }
}
